import { extend } from 'vee-validate'

// 전화번호
export const validatePhoneNumber = (phone) => {
    const regExp = /01\d{8,9}/
    const validatePhoneNumber = regExp.test(phone)
    return validatePhoneNumber
}

export const phoneValidator = extend('phoneValidator', {
    validate: validatePhoneNumber,
    message: '휴대폰 번호 형식이 아닙니다.'
})

// 비밀번호
export const validatePassword = (password) => {
    const regExp = /^(?=.*[A-Za-z])(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/
    const validatePassword = regExp.test(password)
    return validatePassword
}

export const passwordValidator = extend('passwordValidator', {
    validate: validatePassword,
    message: '비밀번호 형식이 아닙니다.'
})

// 공백
export const required = extend('required', {
    ...required,
    message: (field) => { 
        if(field === "회원명" || field === "이메일") {
            return field + '을 입력해주세요.' 
        } else if(field === "근무지") {
            return field + '을 선택해주세요.' 
        } else {
            return field + '를 입력해주세요.' 
        }
    }
})

export const email = extend('email', {
    ...email,
    message: (field) => { 
        return '이메일 형식이 아닙니다.'
    }
})

// 숫자
export const validateNumber = (number) => {
    const regExp = /^[0-9]*$/
    const validateNumber = regExp.test(number)
    return validateNumber
}

export const numberValidator = extend('numberValidator', {
    validate: validateNumber,
    message: '번호 형식이 아닙니다.'
}) 

// 생년월일
export const validateBirthDay = (birth_day) => {
    const regExp = /^\d{4}-\d{2}-\d{2}$/
    const validateBirthDay = regExp.test(birth_day)
    return validateBirthDay
}

export const birthDayValidator = extend('birthDayValidator', {
    validate: validateBirthDay,
    message: '생년월일 형식이 아닙니다.'
})