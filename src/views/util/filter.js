// 핸드폰
export const filterMobile = (mobile) => {
    if (mobile.length === 11) {
        return mobile.slice(0, 3) + "-" + mobile.slice(3, 7) + "-" + mobile.slice(7, 11)
    } else {
        return mobile.slice(0, 3) + "-" +  mobile.slice(3, 6) + "-" + mobile.slice(6, 10)
    }
}

export const filterPhone = (phone) => {
    let len = phone.length
    if(phone.slice(0, 2) === '02') {
        if(len === 10) {
            return '02-' + phone.slice(2, 6) + "-" + phone.slice(6, 10)
        } else {
            return '02-' + phone.slice(2, 5) + "-" + phone.slice(5, 9)
        }
    } else {
        if(len === 11) {
            return phone.slice(0, 3) + "-" + phone.slice(3, 7) + "-" + phone.slice(7, 11)
        } else {
            return phone.slice(0, 3) + "-" + phone.slice(3, 6) + "-" + phone.slice(6, 10)
        }
    }
}
