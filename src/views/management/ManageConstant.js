export default {
    labels: {
        id: 'NO',
        uid: '이메일',
        name: '이름',
        majorJob: '주 업무',
        dealerNum: '종사원 번호',
        joinDd: '입사일',
        workPlaceNm: '근무지',
        role: '권한',
        birthDd: '생년월일',
        mobile: '연락처',
        extensionNum: '내선번호',
        email: '이메일',
        modify: '수정',
        stts: '상태'
    }
}