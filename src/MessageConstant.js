export default {
    // 공통
    SUCCESS_SAVE: '저장되었습니다.',
    FAILED_SAVE: '저장에 실패하였습니다.',
    SUCCESS_UPDATE: '수정되었습니다.',
    FAILED_UPDATE: '수정에 실패하였습니다.',
    SUCCESS_DELETE: '삭제되었습니다.',
    FAILED_DELETE: '삭제에 실패하였습니다.',
    CHECK_DUPLICATE_ID: '아이디 중복 확인을 해주세요.',
    DUPLICATE_ID: '중복된 아이디입니다.',
    NOT_DUPLICATE_ID: '사용가능한 아이디입니다.',
    FAILED_SEARCH: '조회에 실패하였습니다.',
    SUCCESS_LEAVE: '퇴사처리되었습니다.',
    FAILED_LEAVE: '퇴사 처리에 실패하였습니다.',

    // 직원 등록 메시지
    FAILED_LOAD_MANAGER: '직원정보를 불러오지 못했습니다.',
    SELECT_WORK_PLACE: '근무지를 선택해주세요.'
    

    // this.$store.commit('mall/' + Constant.MAIN_SNACKBAR, {msg : MessageConstant.NOT_DUPLICATE_ID, me: this});
}