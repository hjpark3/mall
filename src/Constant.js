export default {
    BASEURL: 'http://172.30.1.7:9090',
    LOGIN: 'login',
    LOGOUT: 'logout',
    // 직원 관리
    LOAD_MANAGER_LIST: 'loadManagerList',
    LOAD_MANAGER: 'loadManager',
    SAVE_MANAGER: 'saveManager',
    UPDATE_MANAGER: 'updateManager',
    LEAVE_MANAGER: 'leaveManager',
    LOAD_WORK_PLACE: 'loadWorkPlace',
    SAVE_WORK_PLACE: 'saveWorkPlace',
    DELETE_WORK_PLACE: 'deleteWorkPlace',

    // 차량 모델 관리
    LOAD_CAR_MODEL_TREE: 'loadCarModelTree',
    
    // 공용
    CHECK_DUPLICATE_ID: 'checkDuplicateId',

    
    LOAD_LIST : 'loadList',
    APPEND_LIST : 'appendList',
    CATEGORY_LIST : 'categoryList',
    TOGGLE_MENU : 'toggleMenu',
    SET_TITLE : 'setTitle',
    SET_INFINITE : 'setInfinite',
    FIND_CONTENT : 'findContent',
    APPEND_CHANNEL_LIST : 'appendChannelList',
    LOAD_COMMENT : 'loadComment',
    ADD_COMMENT : 'addComment',
    APPEND_COMMENT : 'appendComment',
    LOGIN_SUCCESS : 'loginSuccess',
    SET_USER_INFO : 'setUserInfo',
    LIKE_UNLIKE : 'likeUnlike',
    SET_LIKE_UNLIKE : 'setLikeUnlike',
    SET_SUBSCRIBE : 'setSubscribe',
    SEARCH_YOUTUBE : 'searchYoutube',
    SET_YOUTUBE_LOADING : 'setYoutubeLoading',
    SAVE_CONTENT : 'saveContent',
    EMPTY_YOUTUBE_CONTENTS : 'emptyYoutubeContents',
    SET_CURRENT_PAGE : 'setCurrentPage',
    LOGIN_SNACKBAR : 'loginSnackBar',
    LOGIN_SNACKBAR_FLAG : 'loginSnackBarFlag',
    MAIN_SNACKBAR : 'mainSnackBar',
    MAIN_SNACKBAR_FLAG : 'mainSnackBarFlag',
    MENU_LIST : 'menuList'
}