import axios from 'axios';
import Constant from '@/Constant';
import router from '@/router'

let BASEURL = Constant.BASEURL;

export default {
     // 로그인
    [Constant.LOGIN]: async (store, payload) => {
        try {
            let login = (await axios.post(`${BASEURL}/v1/signin`,{ id : payload.userId, password : payload.userPwd })).data
            if (login.success) {
                // console.log(login)
                store.commit(Constant.LOGIN_SUCCESS, login)
                router.push({path: '/manager/list'}).catch(()=>{})
            }
        } catch (err) {
            console.log("로그인 에러 : ", err)
        }
    },

    // 유저 리스트
    [Constant.LOAD_MANAGER_LIST]: async (store, payload) => {
        let token = store.state.token

        try {
            let returnValue = (await axios.post(`${BASEURL}/v1/user/search`, payload, {
                headers : {
                    "X-AUTH-TOKEN" : token
                }
            })).data

            if (returnValue.success) {
                return returnValue
            } else {
                return false
            }
        } catch (err) {
            console.log("유저 리스트 : ", err)
        }
    },

    // 유저 조회
    [Constant.LOAD_MANAGER]: async (store, payload) => {
        let token = store.state.token
        let param = {}
        param.pageSize = 1
        param.searchType = 'id'
        param.keyword = payload.id

        try {
            let returnValue = (await axios.post(`${BASEURL}/v1/user/search`, param, {
                headers : {
                    "X-AUTH-TOKEN" : token
                }
            })).data

            if (returnValue.success) {
                return returnValue
            } else {
                return false
            }
        } catch (err) {
            console.log("유저 조회 : ", err)
        }
    },

    // 유저 ID 중복 체크
    [Constant.CHECK_DUPLICATE_ID]: async (store, payload) => {
        try {
            let returnValue = (await axios.get(`${BASEURL}/v1/user/check?uid=` + payload.uid)).data
            if (returnValue.success) {
                return returnValue
            } else {
                return false
            }
        } catch (err) {
            console.log("유저 중복 체크 : ", err)
        }
    },

    // 유저 저장
    [Constant.SAVE_MANAGER]: async (store, payload) => {
        let token = store.state.token
        let param = [payload]

        try {
            let returnValue = (await axios.put(`${BASEURL}/v1/user`, param, {
                headers : {
                    "X-AUTH-TOKEN" : token
                }
            })).data

            if (returnValue.success) {
                return true
            } else {
                return false
            }
        } catch (err) {
            console.log("유저 수정 : ", err)
        }
    },

    // 유저 수정
    [Constant.UPDATE_MANAGER]: async (store, payload) => {
        let token = store.state.token
        let param = [payload]

        try {
            let returnValue = (await axios.put(`${BASEURL}/v1/user`, param, {
                headers : {
                    "X-AUTH-TOKEN" : token
                }
            })).data

            if (returnValue.success) {
                return returnValue
            } else {
                return false
            }
        } catch (err) {
            console.log("유저 수정 : ", err)
        }
    },

     // 유저 삭제
     [Constant.LEAVE_MANAGER]: async (store, payload) => {
        let token = store.state.token

        try {
            let returnValue = (await axios.put(`${BASEURL}/v1/user/` + payload, {}, {
                headers : {
                    "X-AUTH-TOKEN" : token
                }
            })).data

            if (returnValue.success) {
                return true
            } else {
                return false
            }
        } catch (err) {
            console.log("유저 수정 : ", err)
        }
    },

    // 근무지 리스트
    [Constant.LOAD_WORK_PLACE]: async (store, payload) => {
        let token = store.state.token

        try {
            let returnValue = (await axios.post(`${BASEURL}/common/workplace/search`, payload, {
                headers : {
                    "X-AUTH-TOKEN" : token
                }
            })).data

            if (returnValue.success) {
                return returnValue
            } else {
                return false
            }
        } catch (err) {
            console.log("근무지 리스트 : ", err)
        }
    },

    // 근무지 저장
    [Constant.SAVE_WORK_PLACE]: async (store, payload) => {
        let token = store.state.token

        try {
            let returnValue = (await axios.put(`${BASEURL}/common/workplace`, payload, {
                headers : {
                    "X-AUTH-TOKEN" : token
                }
            })).data

            if (returnValue.success) {
                return returnValue
            } else {
                return false
            }
        } catch (err) {
            console.log("근무지 저장 : ", err)
        }
    },

    // 근무지 삭제
    [Constant.DELETE_WORK_PLACE]: async (store, payload) => {
        let token = store.state.token

        try {
            let returnValue = (await axios.delete(`${BASEURL}/common/workplace/`+ payload, {
                headers : {
                    "X-AUTH-TOKEN" : token
                }
            })).data

            if (returnValue.success) {
                return returnValue
            } else {
                return false
            }
        } catch (err) {
            console.log("근무지 삭제 : ", err)
        }
    },

    // 차량 트리 모델 조회
    [Constant.LOAD_CAR_MODEL_TREE]: async (store, payload) => {
        let token = store.state.token

        try {
            let returnValue = (await axios.post(`${BASEURL}/operation/car/tree`, payload, {
                headers : {
                    "X-AUTH-TOKEN" : token
                }
            })).data

            if (returnValue.success) {
                return returnValue
            } else {
                return false
            }
        } catch (err) {
            console.log("근무지 삭제 : ", err)
        }
    },
}