import axios from 'axios';
import Constant from '@/Constant';
import router from '@/router'
import eventBus from '@/EventBus'

let BASEURL = Constant.BASEURL;

export default {

    [Constant.SEARCH_YOUTUBE] : (store,payload) => {
        let token = store.state.token;
        let flag = payload.flag;
        store.commit(Constant.SET_YOUTUBE_LOADING,true);
        axios.get(`${BASEURL}/api/youtube`,{
            params : payload,
            headers : {
                "Authorization" : "Bearer " + token
            }
        })
        .then(({data}) =>{
            data.flag = flag;
            store.commit(Constant.SET_YOUTUBE_LOADING,false);
            store.commit(Constant.SEARCH_YOUTUBE,data)
        })
        .catch(({response}) =>{
            store.commit(Constant.SET_YOUTUBE_LOADING,false);
            if(response != undefined && response != null && response.data) {
            let result = response.data;
            alert(result.body.message.substring(result.body.message.indexOf(":")+1));
            }
        })
    },
    // 컨텐츠 목록 조회
    [Constant.LOAD_LIST] : (store, payload)=> {
        // 로딩상태 띄우기
        let token = store.state.token;
        let options = { 
            params : payload.params
        }
        if(token != undefined && token != null && token != "") {
            options.headers = {
                "Authorization" : "Bearer " + token
                }
        }
        axios.get(`${BASEURL}/contents`, options)
        .then(({data})=>{
            let obj = {contents : data.data, feed : payload.params.feed };
            store.commit(Constant.LOAD_LIST, obj);
            if(payload.params.categorySeq != undefined && payload.params.categorySeq != null && payload.params.categorySeq != "") {
                store.commit(Constant.SET_TITLE,{categorySeq : payload.params.categorySeq});
            }
        })
        .catch(({response})=> {
            if(response.data.body.statusCode == 401) {
                store.commit(Constant.MAIN_SNACKBAR,{msg : response.data.body.message});
            }
            store.commit(Constant.LOAD_LIST, {contents : [] , feed : payload.params.feed});
        })
    },
    // 컨텐츠목록 삽입
    [Constant.APPEND_LIST] : (store, payload)=> {
        let token = store.state.token;
        let options = { 
            params : payload.params
        }
        if(token != undefined && token != null && token != "") {
            options.headers = {
                "Authorization" : "Bearer " + token
                }
        }
        axios.get(`${BASEURL}/contents`, options)
        .then(({data})=>{
            let obj = {contents : data.data, feed : payload.params.feed, limit : payload.params.limit, $state : payload.$state };
            store.commit(Constant.APPEND_LIST, obj);
        })
        .catch((error)=> {
            console.log("APPEND_LIST 에러 : ", error);
            // 로딩상태 종료
        })
    },
    // 카테고리 목록 조회
    [Constant.CATEGORY_LIST] : (store,payload) =>{
        axios.get(`${BASEURL}/categories`)
        .then(({data})=>{
            store.commit(Constant.CATEGORY_LIST, {dynamic : data.data, static : payload.list});
        })
        .catch((error)=> {
            console.log("CATEGORY_LIST 에러 : ", error);
            // 로딩상태 종료
        })
    },
    // 상세 컨텐츠 조회
    [Constant.FIND_CONTENT] : (store,payload) =>{
        axios.get(`${BASEURL}/content`,payload)
        .then(({data})=>{
            store.commit(Constant.FIND_CONTENT, data.body);
        })
        .catch((error)=> {
            console.log("FIND_CONTENT 에러 : ", error);
            // 로딩상태 종료
        })
    },
    // 상세 컨텐츠에 표출되는 목록 조회
    [Constant.APPEND_CHANNEL_LIST] : (store,payload) =>{
        axios.get(`${BASEURL}/channel/contents`,{params : payload.params})
        .then(({data})=>{
            store.commit(Constant.APPEND_CHANNEL_LIST, {popular : data.data,limit : payload.params.limit,$state : payload.$state});
        })
        .catch((error)=> {
            console.log("CATEGORY_LIST 에러 : ", error);
            // 로딩상태 종료
        })
    },
    // 댓글 목록 조회
    [Constant.LOAD_COMMENT] : (store,payload) => {
        axios.get(`${BASEURL}/comments`,{params : payload.params})
        .then(({data})=>{
            store.commit(Constant.LOAD_COMMENT, {comments : data.data,limit : payload.params.limit});
        })
        .catch((error)=> {
            console.log("LOAD_COMMENT 에러 : ", error);
            // 로딩상태 종료
        })
    },
    // 댓글 목록 조회(삽입)
    [Constant.APPEND_COMMENT] : (store,payload) => {
        axios.get(`${BASEURL}/comments`,{params : payload.params})
        .then(({data})=>{
            store.commit(Constant.APPEND_COMMENT, {comments : data.data,limit : payload.params.limit,$state : payload.$state});
        })
        .catch((error)=> {
            console.log("APPEND_COMMENT 에러 : ", error);
            // 로딩상태 종료
        })
    },
    // 댓글 등록
    [Constant.ADD_COMMENT] : (store,payload) => {
        let token = store.state.token;
        axios.post(`${BASEURL}/api/comment`,payload,{
            headers : {
            "Authorization" : "Bearer " + token
            }
        })
        .then(({data}) =>{
            if(data.success) {
                store.dispatch(Constant.LOAD_COMMENT,{ params : {start : 0, limit : 8, contentsSeq : payload.contentsSeq}})
            }
        })
        .catch(({response}) =>{
            if(response != undefined && response != null && response.data) {
            let result = response.data;
            alert(result.body.message.substring(result.body.message.indexOf(":")+1));
            }
        })
    },
    // 좋아요,싫어요 저장
    [Constant.LIKE_UNLIKE] : (store,payload) => {
        let token = store.state.token;
        axios.post(`${BASEURL}/api/content/like`,payload,{
            headers : {
            "Authorization" : "Bearer " + token
            }
        })
        .then(({data})=>{
            store.commit(Constant.SET_LIKE_UNLIKE, data.body);
        })
        .catch((error)=> {
            console.log("LIKE_UNLIKE 에러 : ", error);
            // 로딩상태 종료
        })
    },
    // 구독처리
    [Constant.SET_SUBSCRIBE] : (store) => {
        let token = store.state.token;
        let channelSeq = store.state.content.channelSeq;
        axios.post(`${BASEURL}/api/subscribe`,{channelSeq : channelSeq},{
            headers : {
            "Authorization" : "Bearer " + token
            }
        })
        .then(({data})=>{
            store.commit(Constant.SET_SUBSCRIBE, data.body);
        })
        .catch((error)=> {
            console.log("SET_SUBSCRIBE 에러 : ", error);
            // 로딩상태 종료
        })
    }
}