// import Vue from 'vue';
import Constant from '@/Constant';
import router from '@/router'
export default {
    // 로그인 객체 설정
    [Constant.SET_USER_INFO] : (state,payload) => {
        if(payload != undefined && payload != null && payload != "") {
            state.userInfo = payload;
            state.token = payload.token;
        }
    },
    // 로그아웃
    [Constant.LOGOUT] : (state) => {
        localStorage.removeItem('userInfo');
        state.userInfo = null;
        state.token = null;
        router.push({path : '/'}).catch(()=>{});
    },
    // 목록 제거
    [Constant.EMPTY_YOUTUBE_CONTENTS] : (state) => {
        state.youtube_contents = [];  
    },
    // 로그인 성공
    [Constant.LOGIN_SUCCESS] : (state, payload)=> {
        let result = payload.data
        let token = result.token
        let user_name = result.user.name
        let user_uid = result.user.uid
        let user_id = result.user.id
        let auth = result.user.role
        let company_id = result.user.companyId
        let userInfo = {
            token : token,
            user_name : user_name,
            user_id : user_id,
            user_uid : user_uid,
            company_id : company_id,
            auth : auth
        }
        localStorage.setItem('userInfo',JSON.stringify(userInfo));
        state.userInfo = userInfo;
        state.token = token;
    },
    
    // 직원현황 조회
    [Constant.LOAD_MANAGER_LIST]: (state, payload) => {
        state.managerList = payload.list
    },

    // 컨텐츠 상태 조회
    [Constant.LOAD_LIST] : (state, payload)=> {
         if(payload.feed != undefined && payload.feed != null && payload.feed != ""){
            if(payload.feed == "top") {
                state.main_contents = payload.contents;        
            }
            else {
                state.contents = payload.contents;
            }
        }
    },
    // 컨텐츠 상태 삽입
    [Constant.APPEND_LIST] : (state, payload)=> {
        if(payload.contents != undefined && payload.contents != null) {
            if(payload.contents.length > 0 && payload.limit == payload.contents.length) {
                if(payload.$state) {
                    state.contents = state.contents.concat(payload.contents);
                    payload.$state.loaded();
                }
            }
            else {
                if(payload.contents.length > 0) {
                    state.contents = state.contents.concat(payload.contents)
                }
                if(payload.$state) {
                    payload.$state.complete();
                }
            }
        }
        else {
            if(payload.$state) {
                payload.$state.complete();
            }
        }
    },
    // 메뉴 카테고리 목록 조회
    [Constant.CATEGORY_LIST] : (state,payload) => {
        state.categories = payload.dynamic;
        let list = payload.static;
        list.push({
            header: '인기 YOUTUBE'
        })
        for(let i =0; i< payload.dynamic.length; i++) {
            let obj = {
                title : payload.dynamic[i].categoryName,
                route : {
                    name : 'category_' + payload.dynamic[i].categorySeq,
                    params : {categorySeq : payload.dynamic[i].categorySeq}
                }
            };
            switch(payload.dynamic[i].categoryIcon) {
                case 'music' :
                    obj.icon = 'MusicIcon';
                    break;
                case 'gamepad-square' :
                    obj.icon = 'MailIcon';
                    break;
                case 'filmstrip' : 
                    obj.icon = 'FilmIcon';
                    break;
                case 'newspaper' : 
                    obj.icon = 'LayoutIcon';
                    break;
                case 'access-point' : 
                    obj.icon = 'AirplayIcon';
                    break;
                case 'lightbulb-outline' : 
                    obj.icon = 'BookOpenIcon';
                    break;
                case 'sunglasses' : 
                    obj.icon = 'PlayCircleIcon';
                    break;   
            }
            list.push(obj)
        }
        state.menuList = list;
    },
    // 메뉴 카테고리 별 타이틀 조회
    [Constant.SET_TITLE] : (state,payload) => {
        if(payload != undefined && payload != null && payload != "") {
            let categories = state.categories;
            if(categories != undefined && categories != null && categories.length > 0 && payload.categorySeq != undefined && payload.categorySeq != null && payload.categorySeq > 0) {
                let index = categories.findIndex((d) =>{
                    return d.categorySeq == Number(payload.categorySeq);
                })
                if(index > -1) {
                    state.title = categories[index].categoryName;
                }
            }
            else {
                state.title = payload.title;
            }
        }
    },
    [Constant.SET_INFINITE] : (state,payload) =>{
        state.infinite = payload;
    },
    [Constant.APPEND_CHANNEL_LIST] : (state,payload) =>{
        if(payload.popular != undefined && payload.popular != null) {
            if(payload.popular.length > 0 && payload.limit == payload.popular.length) {
                if(payload.$state) {
                    state.popular = state.popular.concat(payload.popular)
                    payload.$state.loaded();
                }
            }
            else {
                if(payload.popular.length > 0) {
                    state.popular = state.popular.concat(payload.popular)
                }
                if(payload.$state) {
                    payload.$state.complete();
                }
            }
        }
    },
    [Constant.LOAD_COMMENT] : (state,payload) => {
        if(payload.comments != undefined && payload.comments != null) {
            if(payload.comments.length > 0) {
                state.comments = payload.comments
            }
        }
    },
    [Constant.APPEND_COMMENT] : (state,payload) => {
        if(payload.comments != undefined && payload.comments != null) {
            if(payload.comments.length > 0 && payload.limit == payload.comments.length) {
                if(payload.$state) {
                    state.comments = state.comments.concat(payload.comments)
                    payload.$state.loaded();
                }
            }
            else {
                if(payload.comments.length > 0) {
                    state.comments = state.comments.concat(payload.comments)
                }
                if(payload.$state) {
                    payload.$state.complete();
                }
            }
        }
    },
    [Constant.SET_LIKE_UNLIKE] : (state,payload) => {
       state.like = payload.like;
       state.unlike = payload.unlike;
    },
    [Constant.SET_SUBSCRIBE] : (state,payload) => {
        state.totalSubscribe = payload.totalSubscribe;
        state.subscribe = payload.subscribeFlag == "SUBSCRIBE" ? 1 : 0
     },
     [Constant.SET_YOUTUBE_LOADING] : (state,payload) => {
        state.youtube_loading = payload;
     },
     [Constant.SEARCH_YOUTUBE] : (state,payload) => {
        if(payload.nextPageToken != undefined && payload.nextPageToken != null && payload.nextPageToken != "") {
            state.next_token = payload.nextPageToken;
        }
        else {
            state.next_token = "";
        }
        if(payload.prevPageToken != undefined && payload.prevPageToken != null && payload.prevPageToken != "") {
            state.prev_token = payload.prevPageToken;
        }
        else {
            state.prev_token = "";
        }
        state.youtube_contents = payload.items;
        state.total_page = Math.ceil(payload.pageInfo.totalResults / payload.pageInfo.resultsPerPage);
        if(payload.flag == "first") {
            state.current_page = 1;
        }
        else if(payload.flag == "next") {
            state.current_page += 1;
        }
        else if(payload.flag == "prev") {
            state.current_page -= 1;
        }
     },
     // SET_CURRENT_PAGE SET
     [Constant.SET_CURRENT_PAGE] : (state,payload) =>{
         state.current_page = payload;
     },
     [Constant.LOGIN_SNACKBAR] : (state,payload) =>{
        state.login_msg = payload.msg;
        state.login_snackbar = true;
        payload.me.$bvToast.toast(`${payload.msg}`,{
            title: '알 림',
            toaster : 'b-toaster-top-center',
            solid: true
        })
     },
     [Constant.LOGIN_SNACKBAR_FLAG] : (state,payload) =>{
        state.login_msg = payload.msg;
        state.login_snackbar = payload.snackbar;
     },
     [Constant.MAIN_SNACKBAR] : (state,payload) =>{
        state.main_msg = payload.msg;
        state.main_snackbar = true;
        payload.me.$bvToast.toast(`${payload.msg}`,{
            title: '알 림',
            toaster : 'b-toaster-top-center',
            solid: true
        })
     },
     [Constant.MAIN_SNACKBAR_FLAG] : (state,payload) =>{
        state.main_msg = payload.msg;
        state.main_snackbar = payload.snackbar;
     },
    // 메뉴 카테고리 별 타이틀 조회
    [Constant.FIND_CONTENT] : (state,payload) => {
        if(payload != undefined && payload != null && payload != "") {
            let like = payload.like;
            let unlike = payload.unlike;
            let subscribe = payload.subscribe;
            let totalSubscribe = payload.totalSubscribe;
            let viewCount = payload.viewCount;
            let popular = payload.popular;
            let comments = payload.comments;

            delete payload.like;
            delete payload.unlike;
            delete payload.subscribe;
            delete payload.totalSubscribe;
            delete payload.viewCount;
            delete payload.popular;
            delete payload.comments;

            state.content = payload;
            
            state.like = like;
            state.unlike = unlike;
            state.subscribe = subscribe;
            state.totalSubscribe = totalSubscribe;
            state.viewCount = viewCount;
            state.popular = popular;
            state.comments = comments;

            let categories = state.categories;
            if(categories != undefined && categories != null && categories.length > 0 && payload.categorySeq != undefined && payload.categorySeq != null && payload.categorySeq > 0) {
                let index = categories.findIndex((d) =>{
                    return d.categorySeq == Number(payload.categorySeq);
                })
                if(index > -1) {
                    state.title = categories[index].categoryName;
                }
            }
            else {
                state.title = payload.title;
            }
            
        }
    }
}