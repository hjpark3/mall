import state from './state'
import actions from './actions'
import manageActions from './management_actions'
import mutations from './mutations'
export default {
    namespaced: true,
    state : state,
    actions : {
        ...actions, 
        ...manageActions
    },
    mutations : mutations
}