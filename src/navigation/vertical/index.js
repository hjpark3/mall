export default [
  {
    title: '직원 관리',
    icon: 'HomeIcon',
    children: [
      { title: '직원 현황', route: 'manager-list' },
      { title: '직원 등록', route: 'manager-reg' },
      { title: '권한 관리', route: 'manager-auth' },
    ]
  }, {
    title: '운영 관리',
    icon: 'HomeIcon',
    children: [
      { title: '차량 모델 관리', route: 'operation-car' },
      { title: '배너 관리', route: 'operation-banner' }
    ]
  }
]
