import Vue from 'vue'
import VueRouter from 'vue-router'
import Error404 from '@/views/error/Error404'
import Mall from '@/views/mall/Main'
import MainView from '@/views/mall/main/MainView'
import MallRegister from '@/views/mall/register/FormWizard'
import MallPurchase from '@/views/mall/purchase/Purchase'
// 관리 컴포넌트
import Management from '@/views/management/Management'
import ManagerList from '@/views/management/manager/List'
import ManagerReg from '@/views/management/manager/regist/Reg'
import Authority from '@/views/management/manager/authority/Authority'
import CarModel from '@/views/management/operation/car-model/CarModel'
import Banner from '@/views/management/operation/Banner'
//로그인
import Login from '@/views/login/Login'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      name: 'main',
      path: '/',
      component: MainView
    },
    {
      name: 'manager-management',
      path: '/manager',
      component: Management,
      children: [{
        name: 'manager-list',
        path: 'list',
        component: ManagerList,  
      }, {
        name: 'manager-reg',
        path: 'reg',
        component: ManagerReg
      }, {
        name: 'manager-auth',
        path: 'auth',
        component: Authority
      }
    ]},
    {
      name: 'operation-management',
      path: '/operation',
      component: Management,
      children: [{
        name: 'operation-car',
        path: 'car',
        component: CarModel
      }, {
        name: 'operation-banner',
        path: 'banner',
        component: Banner
      }]
    },
    {
      name: 'login',
      path: '/login',
      component: Login
    },
    {
      path: '/error-404',
      name: 'error-404',
      component: Error404,
      meta: {
        layout: 'full',
      },
    },
    {
      path: '*',
      redirect: 'error-404',
    },
    {
      name: 'mall',
      path: '/mall',
      component: Mall,
      children: [{
        path: 'main',
        component: MainView
      },{
        path: 'purchase',
        component: MallPurchase,
        query: {
          keyword: ''
        }
      }]
    },
    {
      name: 'mall/register',
      path: '/mall/register',
      component: MallRegister
    }
  ],
})

export default router
