import Vue from 'vue'
import { BootstrapVue, BootstrapVueIcons}  from 'bootstrap-vue'
import VueCompositionAPI from '@vue/composition-api'
import VueYoutube from 'vue-youtube'
import VueMoment from 'vue-moment'
import VueFormWizard from 'vue-form-wizard'

import router from './router'
import store from './store'
import App from './App.vue'

// Global Components
import './global-components'

// 3rd party plugins
import '@/libs/portal-vue'
import '@/libs/toastification'

// BSV Plugin Registration
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
// Composition API
Vue.use(VueCompositionAPI)
Vue.use(VueYoutube)
Vue.use(VueMoment)
Vue.use(VueFormWizard)
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import 'bootstrap/dist/css/bootstrap.min.css'


// import core styles
require('@core/scss/core.scss')

// import assets styles
require('@/assets/scss/style.scss')

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
